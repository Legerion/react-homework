import React from 'react';
import {Link} from "react-router-dom";
import "./Header.scss"

const Header = () => {
    return (
        <header className="App__nb">
            <div className="App__logo-text">
                <Link className="App__logo-text" to="/">
                    VINO.UA
                </Link>
            </div>
            <div className='nb__wrapper-link'>
                <Link className='nb__link' to="/">
                    Главная
                </Link>
                <Link to="/cart" className='nb__link'>
                    Корзина
                </Link>
                <Link to="/favorites" className='nb__link'>
                    Избраное
                </Link>
            </div>
        </header>
    );
}

export default Header;