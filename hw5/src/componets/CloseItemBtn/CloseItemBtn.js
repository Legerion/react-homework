import React from 'react';
import Button from "../Button/Button";

const CloseItemBtn = (props) => {
    const {className, onClick, text} = props
    return (
        <Button
            className={className}
            text={text}
            backgroundColor=""
            onClick={onClick}
        />
    );
}

export default CloseItemBtn;