import React from 'react';
import "./ListItems.scss"
import CardItem from "../CardItem/CardItem";
import PropTypes from "prop-types";
import {v4 as uuidv4} from 'uuid';

const ListItems = (props) => {
    const {itemClassName: className, productsList, displayModal, closeModal, inCart} = props;

    return (
        <div className="list-item">
            {productsList.map(el => {
                return <CardItem className={className}
                                    name={el.name}
                                    price={el.price}
                                    src={el.src}
                                    key={uuidv4()}
                                    article={el.article}
                                    color={el.color}
                                    displayModal={displayModal}
                                    closeModal={closeModal}
                                    inCart={inCart}
                />})}
        </div>
    );
}

ListItems.propTypes = {
    productsList: PropTypes.arrayOf(PropTypes.object).isRequired,
    displayModal: PropTypes.func.isRequired
}

export default ListItems;