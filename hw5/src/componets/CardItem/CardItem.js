import React, {useEffect, useState} from 'react';
import "./CardItem.scss"
import Button from "../Button/Button";
import PropTypes from "prop-types";
import CloseItemBtn from "../CloseItemBtn/CloseItemBtn";
import {favIconUnpicked, favIconPicked} from "../../utils/IconsLinks"

const CardItem = (props) => {
    const [inFavorites, setInFavorites] = useState(false)

    useEffect(() => {
        const {article} = props
        const localFavoritesArr = JSON.parse(localStorage.getItem("favorites")) || [];
        const isFavoriteBool = localFavoritesArr.some(fav => fav.article === article);
        setInFavorites(isFavoriteBool)
    }, [props])

    const addToFavorites = (itemProps) => {
        let storageFavorites = localStorage.getItem("favorites");
        if (!storageFavorites) {
            localStorage.setItem("favorites", JSON.stringify([itemProps]));
        }
        if (storageFavorites) {
            if (storageFavorites.includes(JSON.stringify(itemProps))) {
                let parsedStorageFavorites = JSON.parse(storageFavorites)
                let updatedStorageFavorites = parsedStorageFavorites.filter(e => e.article !== itemProps.article);
                localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites))
                props.displayModal("removeFav", props)
            } else {
                let updatedStorageFavorites = JSON.parse(localStorage.getItem("favorites"));
                updatedStorageFavorites.push(itemProps);
                localStorage.setItem("favorites", JSON.stringify(updatedStorageFavorites));
            }
        }
    }

    const {className, name, price, src, article, color, displayModal, inCart} = props;
    return (
        <div className={className}>

            {inCart && <CloseItemBtn
                className="btn btn__item--delete"
                text="+"
                onClick={() => {
                    displayModal("removeModal", props)
                }}
            />}

            <div className="product-item__cont">
                <img className="product-item-img" src={src} alt={`${name}`}/>
                <img
                    className="img__add-to-favorites"
                    alt="add-to-favorites"
                    src={inFavorites ? favIconPicked
                        : favIconUnpicked}
                    onClick={() => {
                        setInFavorites(!inFavorites)
                        addToFavorites(props)
                    }}
                />
            </div>
            <div className="product-item__title">
                <h4>{name}</h4>
                <h5>Цвет: {color}</h5>
                <h5 className='product-item__price'>${price}</h5>
                <h5 className='product-item__article'>Артикль: {article}</h5>
            </div>

            {!inCart && <div className="product-item__buttons-container">
                <Button
                    className="btn btn__add-to-cart"
                    text="Добавить"
                    backgroundColor="#1dbc18"
                    onClick={() => {
                        displayModal("cartModal", props, inFavorites)
                    }}
                />

            </div>}

            {inCart && <div className="product-item__buttons-container">
                <Button
                    className="btn btn__add-to-cart"
                    text="Удалить"
                    backgroundColor="#1dbc18"
                    onClick={() => {
                        displayModal("removeModal", props)
                    }}
                />

            </div>}
        </div>
    );
};

CardItem.propTypes = {
    className: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    displayModal: PropTypes.func.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string
}

CardItem.defaultProps = {
    color: "no info"
}

export default CardItem;