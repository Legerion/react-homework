import React from 'react';
import "./Button.scss"
import PropTypes from "prop-types"

const Button = (props) => {
    const {type, disabled, className, text, backgroundColor, onClick} = props
    return (
        <button type={type}
                disabled={disabled}
                className={className}
                style={{backgroundColor: backgroundColor}}
                onClick={onClick}
        >{text}</button>
    );
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string.isRequired,
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    className: "btn",
    text: "Button",
    backgroundColor: "orangered"
}

export default Button;