import {combineReducers} from "redux";
import reducer from "../server/reducer";
import {modalTypeReducer, modalTargetReducer} from "../modal/reducer";
import checkoutFormReducer from "../checkoutForm/checkoutFormReducer";

export default combineReducers({
    productsList: reducer,
    openModal: modalTypeReducer,
    currentTargetProps: modalTargetReducer,
    checkoutFormValues: checkoutFormReducer

})
