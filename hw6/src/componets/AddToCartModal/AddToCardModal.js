import React from 'react';
import ModalTemplate from "../ModalTemplate/ModalTemplate"
import Button from "../Button/Button";
import {v4 as uuidv4} from 'uuid';
import PropTypes from "prop-types";


const AddToCartModal = ({closeModal, currentTargetProps}) => {

    const addToCart = (itemProps) => {
        let storageCart = localStorage.getItem("cart");
        if (!storageCart) {
            localStorage.setItem("cart", JSON.stringify([itemProps]));
        } else {
            let updatedStorageCart = JSON.parse(localStorage.getItem("cart"));
            updatedStorageCart.push(itemProps);
            localStorage.setItem("cart", JSON.stringify(updatedStorageCart));
        }
    }

    return (
        <ModalTemplate className="modal"
                       data-testid="addToCartTestModal"
                       header="Вы хотите добавить товар в корзину?"
                       closeButton={true}
                       closeModal={closeModal}
                       currentTargetProps={currentTargetProps}

                       actions={[
                           <Button className="btn btn__modal--manipulate"
                                   text="ДА"
                                   backgroundColor="#1dbc18"
                                   key={uuidv4()}
                                   onClick={() => {
                                       addToCart(currentTargetProps)
                                       closeModal()
                                   }}
                           />,

                           <Button className="btn btn__modal--manipulate"
                                   text="НЕТ"
                                   backgroundColor="#1dbc18"
                                   key={uuidv4()}
                                   onClick={() => {
                                       closeModal()
                                   }}
                           />
                       ]}
        />
    );
}

AddToCartModal.propTypes = {
    closeModal: PropTypes.func.isRequired,
    currentTargetProps: PropTypes.object.isRequired
}

export default AddToCartModal;