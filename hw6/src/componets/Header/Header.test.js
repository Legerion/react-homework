import React from "react";
import {render} from "@testing-library/react";
import Header from "./Header";
import {BrowserRouter} from "react-router-dom";

describe("header component tests", () => {
    test("rendering", () => {
        const header = render(
            <BrowserRouter>
                <Header/>
            </BrowserRouter>);

        expect(header).toBeDefined();
        expect(header.getByTestId("shopName").textContent).toBe("Vino.ua");
    });
});