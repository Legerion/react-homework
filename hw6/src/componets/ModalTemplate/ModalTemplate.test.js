import React from "react";
import {render} from "@testing-library/react";
import '@testing-library/jest-dom';
import ModalTemplate from "./ModalTemplate";

describe("modal template tests", () => {
    test("rendering", () => {
        const modalTemplate = render(<ModalTemplate className="testClassName"
                                                    closeButton={true}
                                                    header="test header"
                                                    actions={[]}
                                                    closeModal={() => {
                                                    }}
                                                    currentTargetProps={{
                                                        className: "testClassName",
                                                        title: "test ChocolateBar"
                                                    }}
            />
        );

        expect(modalTemplate).toBeDefined();
    });
});