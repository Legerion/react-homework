import React from 'react';
import {ErrorMessage, Field, Form, Formik} from "formik";
import Card from "@material-ui/core/Card";
import {CardContent, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {boolean, number, object, string} from "yup";
import FormGroup from "@material-ui/core/FormGroup";
import Button from "../Button/Button";
import {connect} from "react-redux";
import {checkoutFormData} from "../../store/checkoutForm/checkoutFormActions";

const CheckoutForm = ({checkoutFormValues, checkoutFormData}) => {
    return (
        <Card>
            <CardContent>
                <Typography variant="h4">Информация для заказа</Typography>
                <Formik validationSchema={
                    object({
                        firstName: string().required("Заполните поле").min(2).max(20),
                        secondName: string().required("Заполните поле").min(2).max(20),
                        age: number().required("Заполните поле").min(18).max(100),
                        address: string().required("Заполните поле").min(20).max(50),
                        phone: number().required("Заполните поле"),
                        termsConditions: boolean().oneOf([true])
                    })
                }
                        initialValues={checkoutFormValues}
                        onSubmit={(values) => {
                            setTimeout(() => {
                                console.log("Clients information:", values);
                                console.log("Purchased items", JSON.parse(localStorage.getItem("cart")).map(e => e.vendorCode));
                                checkoutFormData(values)
                            }, 3000)
                        }}>
                    {({values, errors, isSubmitting, isValidating}) => (
                        <Form>
                            <FormGroup>
                                <Field name="firstName" as={TextField} label="Имя"/>
                                <ErrorMessage name="firstName"/>

                                <Field name="secondName" as={TextField} label="Фамилия"/>
                                <ErrorMessage name="secondName"/>

                                <Field name="age" type="number" as={TextField} label="Возраст"/>
                                <ErrorMessage name="age"/>

                                <Field name="address" as={TextField} label="Адресс" multiline rows={2}/>
                                <ErrorMessage name="address"/>

                                <Field name="phone" as={TextField} label="Телефон"/>
                                <ErrorMessage name="phone"/>
                            </FormGroup>

                            <FormControlLabel name="termsConditions"
                                              control={<Checkbox />}
                                              onChange={() => {
                                                  values.termsConditions = !values.termsConditions;
                                              }}
                                              label={"Я принимаю правила и условия"}/>
                            <ErrorMessage name="termsConditions"/>

                            <Button type="submit"
                                    disabled={isSubmitting || isValidating}
                                    className="btn btn__checkout--proceed"
                                    text="Купить"
                                    backgroundColor="#1dbc18"
                                    onClick={() => {
                                    }}/>
                        </Form>
                    )}
                </Formik>
            </CardContent>
        </Card>
    )
};

const mapStoreToProps = (store) => {

    return {
        checkoutFormValues: store.checkoutFormValues
    }
}

const mapDispatchToProps = (dispatch) => ({
    checkoutFormData: (values) => dispatch(checkoutFormData(values))
})

export default connect(mapStoreToProps, mapDispatchToProps)(CheckoutForm);