import {favIconPicked, favIconUnpicked} from "./IconsLinks";

describe("constants test", () => {
    test("favorites icons links", () => {
        expect(favIconPicked).toBeDefined();
        expect(favIconPicked).toEqual(expect.stringContaining("https://"));
        expect(favIconPicked).toEqual(expect.stringContaining(".svg"));

        expect(favIconUnpicked).toBeDefined();
        expect(favIconUnpicked).toEqual(expect.stringContaining("https://"));
        expect(favIconUnpicked).toEqual(expect.stringContaining(".svg"));
    });
});