import {getProductsFromServer} from "./serverActions";

describe("server actions tests", () => {
    test("getProductsFromServer function", () => {
        const mockGetProductsFromServer = jest.fn(() => getProductsFromServer());
        mockGetProductsFromServer();
        expect(mockGetProductsFromServer).toHaveReturned();

    });
});