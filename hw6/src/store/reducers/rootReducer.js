import {combineReducers} from "redux";
import reducer from "../server/serverReducer";
import {modalTypeReducer, modalTargetReducer} from "../modal/modalReducer";
import checkoutFormReducer from "../checkoutForm/checkoutFormReducer";

export default combineReducers({
    productsList: reducer,
    openModal: modalTypeReducer,
    currentTargetProps: modalTargetReducer,
    checkoutFormValues: checkoutFormReducer

})
