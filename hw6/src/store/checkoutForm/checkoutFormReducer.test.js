import {CHECKOUT_FORM_SUBMITTING} from "./checkoutFormActions"
import checkoutFormReducer from "./checkoutFormReducer";

describe("checkout form reducer test", () => {
    test("submitting the form", () => {
        const initialStoreFormValues = {
            firstName: "",
            secondName: "",
            age: "",
            address: "",
            phone: "",
            termsConditions: false
        };
        const emptyAction = {type: CHECKOUT_FORM_SUBMITTING};
        const testAction = {
            type: CHECKOUT_FORM_SUBMITTING,
            payload: {
                firstName: "testName",
                secondName: "testSurname",
                termsConditions: undefined
            }
        };
        const fakeAction = {type: "NO_SUCH_ACTION", payload: {data: "no such data"}};

        expect(checkoutFormReducer(initialStoreFormValues, emptyAction)).toHaveProperty("firstName", "");
        expect(checkoutFormReducer(initialStoreFormValues, testAction)).toHaveProperty("firstName", "testName");
        expect(checkoutFormReducer(initialStoreFormValues, testAction)).not.toHaveProperty("termsConditions", "");
        expect(checkoutFormReducer(initialStoreFormValues, fakeAction)).not.toHaveProperty("data", "no such data");
        expect(checkoutFormReducer(initialStoreFormValues, fakeAction)).toBe(initialStoreFormValues);
        expect(checkoutFormReducer(initialStoreFormValues, fakeAction)).toStrictEqual(initialStoreFormValues);
    });
});