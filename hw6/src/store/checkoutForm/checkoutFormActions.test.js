import {checkoutFormData} from "./checkoutFormActions";

describe("checkout form actions tests", () => {
    test("checkoutFormData function", () => {
        const mockCheckoutFormData = jest.fn((values) => checkoutFormData(values));
        mockCheckoutFormData();
        expect(mockCheckoutFormData).toHaveReturned();

    });
});