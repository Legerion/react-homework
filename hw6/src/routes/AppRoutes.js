import React from "react";
import {Route, Switch} from "react-router-dom";
import MainPage from "../pages/MainPage";
import CartPage from "../pages/CartPage";
import FavoritesPage from "../pages/FavoritesPage";

const AppRoutes = ({productsList, displayModal, closeModal}) => {
    return (
        <>
            <Switch>
                <Route
                    exact
                    path="/"
                    render={() => <MainPage
                        productsList={productsList}
                        displayModal={displayModal}
                        closeModal={closeModal}
                    />}
                />
                <Route
                    exact
                    path="/cart"
                    render={() => <CartPage
                        displayModal={displayModal}
                        closeModal={closeModal}
                    />}
                />
                <Route
                    exact
                    path="/favorites"
                    render={() => <FavoritesPage
                        displayModal={displayModal}
                    />}
                />
            </Switch>
        </>
    )
}

export default AppRoutes;