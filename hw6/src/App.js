import React, {useEffect} from 'react';
import './App.scss';
import AddToCartModal from "./componets/AddToCartModal/AddToCardModal";
import RemoveFromCartModal from "./componets/RemoveFromCartModal/RemoveFromCartModal";
import Header from "./componets/Header/Header";
import AppRoutes from "./routes/AppRoutes";
import {connect} from "react-redux";
import {getProductsFromServer} from "./store/server/serverActions";
import {setModalType} from "./store/modal/modalActions";
import {setCurrentTargetProps} from "./store/modal/modalActions";

const App = ({
               productsList,
               getProductsFromServer,
               openModal,
               setModalType,
               currentTargetProps,
               setCurrentTargetProps
             }) => {

  const displayModal = (modalTitle, props) => {
    setModalType(modalTitle)
    setCurrentTargetProps(props)
  }

  const closeModal = () => {
    setModalType(null)
  }

  useEffect(() => {
    getProductsFromServer()
  }, [getProductsFromServer])

  const addToCart = () => {
    if (openModal === "cartModal") {
      return <AddToCartModal
          currentTargetProps={currentTargetProps}
          closeModal={closeModal}/>
    }
  }

  const removeFromCart = () => {
    if (openModal === "removeModal") {
      return <RemoveFromCartModal
          currentTargetProps={currentTargetProps}
          closeModal={closeModal}/>
    }
  }

  return (
      <div className="App">
        <Header/>
        <AppRoutes
            productsList={productsList}
            displayModal={displayModal}
            closeModal={closeModal}
        />
        {addToCart()}
        {removeFromCart()}
      </div>
  );
}

const mapStoreToProps = (store) => {
  return {
    productsList: store.productsList,
    openModal: store.openModal,
    currentTargetProps: store.currentTargetProps
  }
}

const mapDispatchToProps = (dispatch) => ({
  getProductsFromServer: () => dispatch(getProductsFromServer()),
  setModalType: (status) => dispatch(setModalType(status)),
  setCurrentTargetProps: (props) => dispatch(setCurrentTargetProps(props))
})

export default connect(mapStoreToProps, mapDispatchToProps)(App);