import React from 'react';
import ListItems from "../componets/ListItems/ListItems";

const MainPage = (props) => {
    const {productsList, displayModal, closeModal} = props;
    return (
        <ListItems
            itemClassName="product-item__container"
            productsList={productsList}
            displayModal={displayModal}
            closeModal={closeModal}
            inCart={false}
        />
    );
}

export default MainPage;