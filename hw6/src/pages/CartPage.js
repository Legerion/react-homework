import React, {useState} from 'react';
import ListItems from "../componets/ListItems/ListItems";
import Button from  "../componets/Button/Button"
import CheckoutForm from "../componets/CheckoutForm/CheckoutForm";

const CartPage = ({displayModal, closeModal}) => {

    const localStorageCart = JSON.parse(localStorage.getItem("cart"));
    const [checkoutForm, setCheckoutForm] = useState(true);

    if (localStorageCart.length >= 1 && checkoutForm) {
        return (
            <div>
                <ListItems
                    itemClassName="product-item__cart-container"
                    productsList={localStorageCart}
                    displayModal={displayModal}
                    closeModal={closeModal}
                    inCart={true}
                />
                <Button className="btn btn__checkout--proceed"
                        text="Оформить"
                        backgroundColor="#1dbc18"
                        onClick={() => {
                            setCheckoutForm(false)
                        }}/>
            </div>
        );
    } else if (localStorageCart.length >= 1 && !checkoutForm) {
        return <CheckoutForm/>
    } else {
        return (
            <div className="no-items-msg">
                <p>Здесь пока пусто</p>
            </div>
        )
    }
}

export default CartPage;