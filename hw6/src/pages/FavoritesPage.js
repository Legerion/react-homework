import React from 'react';
import ListItems from "../componets/ListItems/ListItems";

const FavoritesPage = ({displayModal}) => {

    const localStorageFavorites = JSON.parse(localStorage.getItem("favorites"));

    if (localStorageFavorites.length >= 1) {
        return (
            <ListItems
                itemClassName="product-item__container"
                productsList={localStorageFavorites}
                displayModal={displayModal}
                inCart={false}
            />
        );
    } else {
        return (
            <div className="no-items-msg">
                <p>Здесь пока пусто</p>
            </div>
        )
    }
}

export default FavoritesPage;